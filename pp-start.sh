#!/bin/sh

# DTR is:
# - PL6/GPIO358 on BH (1.1)
# - PB2/GPIO34 on CE (1.2)

if grep -q 1.1 /proc/device-tree/model
then
	DTR=358
else
	DTR=34
fi

echo "Enabling EG25 WWAN module"
# GPIO35 is PWRKEY
# GPIO68 is RESET_N
# GPIO231 is WAKEUP_IN on BH and AP_READY (active low) on CE
# GPIO232 is W_DISABLE#
for i in 35 68 231 232 $DTR
do
	[ -e /sys/class/gpio/gpio$i ] && continue
	echo $i > /sys/class/gpio/export || return 1
	echo out > /sys/class/gpio/gpio$i/direction || return 1
done

echo 0 > /sys/class/gpio/gpio68/value || return 1
echo 0 > /sys/class/gpio/gpio231/value || return 1
echo 0 > /sys/class/gpio/gpio232/value || return 1
echo 0 > /sys/class/gpio/gpio$DTR/value || return 1

( echo 1 > /sys/class/gpio/gpio35/value && sleep 2 && echo 0 > /sys/class/gpio/gpio35/value ) || return 1

# HACK: don't suspend the USB bus
echo on > /sys/bus/usb/devices/usb3/power/control

# HACK: Install Squeekboard and Wallpaper (Needed to build image with OBS)
if [ ! -f "/home/pine/.pinephone-helpers-firstboot" ]; then
    chmod 777 -R /usr/share/wallpapers/
    sudo -u pine gsettings set org.gnome.desktop.background picture-uri "file:///usr/share/wallpapers/opensuse-background.jpg"
    sudo -u pine gsettings set org.gnome.desktop.interface monospace-font-name "Liberation Mono Regular 10"
    # HACK: Auto-resize FS
    check_root_partition=$(mount | grep " / " | awk NR==1 | awk '{print $1}')
    if [[ "$check_root_partition" == "/dev/mmcblk0p2" ]]; then
        sgdisk -e /dev/mmcblk0
        sgdisk -d 2 /dev/mmcblk0
        sgdisk -N 2 /dev/mmcblk0
        partprobe /dev/mmcblk0
        resize2fs /dev/mmcblk0p2
    fi
fi
echo "1" > /home/pine/.pinephone-helpers-firstboot

