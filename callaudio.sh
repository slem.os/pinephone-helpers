#!/bin/bash
# Call Audio Routing for PinePhone

# Functions
enable_audio_on_call() {

amixer cset name='Headphone Playback Switch' off
amixer cset name='Headphone Source Playback Route' DAC
amixer cset name='Line In Playback Switch' off
amixer cset name='Line Out Playback Switch' off
amixer cset name='Line Out Source Playback Route' Mono Differential
amixer cset name='Mic1 Playback Switch' off
amixer cset name='Mic2 Playback Switch' off
amixer cset name='AIF1 DA0 Playback Volume' 160
amixer cset name='AIF1 DA0 Stereo Playback Route' Mix Mono
amixer cset name='AIF1 Loopback Switch' off
amixer cset name='AIF2 Loopback Switch' off
amixer cset name='AIF2 DAC Playback Volume' 160
amixer cset name='AIF3 Loopback Switch' off
amixer cset name='AIF3 ADC Capture Route' None
amixer cset name='AIF3 DAC Playback Route' None
amixer cset name='DAC Playback Switch' on
amixer cset name='DAC Playback Volume' 160
amixer cset name='DAC Mixer ADC Playback Switch' off
amixer cset name='DAC Mixer AIF1 DA0 Playback Switch' on
amixer cset name='DAC Mixer AIF2 DAC Playback Switch' on
amixer cset name='DAC Reversed Playback Switch' off
amixer cset name='Earpiece Playback Switch' on
amixer cset name='Earpiece Playback Volume' 160
amixer cset name='Earpiece Source Playback Route' DACL

amixer cset name='Line In Capture Switch' off
amixer cset name='Mic1 Capture Switch' on
amixer cset name='Mic1 Boost Volume' 2
amixer cset name='Mic2 Capture Switch' off
amixer cset name='Mic2 Boost Volume' 2
amixer cset name='Mixer Capture Switch' off
amixer cset name='Mixer Reversed Capture Switch' off
amixer cset name='ADC Capture Volume' 160
amixer cset name='ADC Gain Capture Volume' 2
amixer cset name='AIF1 AD0 Capture Volume' 160
amixer cset name='AIF1 AD0 Mixer ADC Capture Switch' on
amixer cset name='AIF2 ADC Capture Volume' 160
amixer cset name='AIF2 ADC Mixer ADC Capture Switch' on
amixer cset name='AIF2 ADC Mixer AIF1 DA0 Capture Switch' off
amixer cset name='AIF2 ADC Mixer AIF2 DAC Rev Capture Switch' off
amixer cset name='AIF2 ADC Mixer AIF1 DA0 Capture Switch' off
amixer cset name='AIF2 ADC Mixer AIF1 DA0 Capture Switch' off

}

enable_generic_audio_profile() {

amixer cset name='Headphone Playback Switch' off
amixer cset name='Headphone Source Playback Route' DAC
amixer cset name='Line In Playback Switch' off
amixer cset name='Line Out Playback Switch' on
amixer cset name='Line Out Playback Volume' 160
amixer cset name='Line Out Source Playback Route' Mono Differential
amixer cset name='Mic1 Playback Switch' off
amixer cset name='Mic2 Playback Switch' off
amixer cset name='AIF1 DA0 Playback Volume' 160
amixer cset name='AIF1 DA0 Stereo Playback Route' Mix Mono
amixer cset name='AIF1 Loopback Switch' off
amixer cset name='AIF2 Loopback Switch' off
amixer cset name='AIF2 DAC Playback Volume' 160
amixer cset name='AIF3 Loopback Switch' off
amixer cset name='AIF3 ADC Capture Route' None
amixer cset name='AIF3 DAC Playback Route' None
amixer cset name='DAC Playback Switch' on
amixer cset name='DAC Playback Volume' 160
amixer cset name='DAC Mixer ADC Playback Switch' off
amixer cset name='DAC Mixer AIF1 DA0 Playback Switch' on
amixer cset name='DAC Mixer AIF2 DAC Playback Switch' on
amixer cset name='DAC Reversed Playback Switch' off
amixer cset name='AIF1 DA0 Stereo Playback Route' Mix Mono
amixer cset name='Earpiece Playback Switch' off
amixer cset name='Earpiece Source Playback Route' DACL

amixer cset name='Line In Capture Switch' off
amixer cset name='Mic1 Capture Switch' on
amixer cset name='Mic1 Boost Volume' 2
amixer cset name='Mic2 Capture Switch' off
amixer cset name='Mic2 Boost Volume' 2
amixer cset name='Mixer Capture Switch' off
amixer cset name='Mixer Reversed Capture Switch' off
amixer cset name='ADC Capture Volume' 160
amixer cset name='ADC Gain Capture Volume' 2
amixer cset name='AIF1 AD0 Capture Volume' 160
amixer cset name='AIF1 AD0 Mixer ADC Capture Switch' on
amixer cset name='AIF2 ADC Capture Volume' 160
amixer cset name='AIF2 ADC Mixer ADC Capture Switch' on
amixer cset name='AIF2 ADC Mixer AIF1 DA0 Capture Switch' off
amixer cset name='AIF2 ADC Mixer AIF2 DAC Rev Capture Switch' off
amixer cset name='AIF2 ADC Mixer AIF1 DA0 Capture Switch' off
amixer cset name='AIF2 ADC Mixer AIF1 DA0 Capture Switch' off

}

enable_generic_audio_profile() {

amixer cset name='Headphone Playback Switch' off
amixer cset name='Headphone Source Playback Route' DAC
amixer cset name='Line In Playback Switch' off
amixer cset name='Line Out Playback Switch' on
amixer cset name='Line Out Playback Volume' 100%
amixer cset name='Line Out Source Playback Route' Mono Differential
amixer cset name='Mic1 Playback Switch' off
amixer cset name='Mic2 Playback Switch' off
amixer cset name='AIF1 DA0 Playback Volume' 160
amixer cset name='AIF1 DA0 Stereo Playback Route' Mix Mono
amixer cset name='AIF1 Loopback Switch' off
amixer cset name='AIF2 Loopback Switch' off
amixer cset name='AIF2 DAC Playback Volume' 160
amixer cset name='AIF3 Loopback Switch' off
amixer cset name='AIF3 ADC Capture Route' None
amixer cset name='AIF3 DAC Playback Route' None
amixer cset name='DAC Playback Switch' on
amixer cset name='DAC Playback Volume' 160
amixer cset name='DAC Mixer ADC Playback Switch' off
amixer cset name='DAC Mixer AIF1 DA0 Playback Switch' on
amixer cset name='DAC Mixer AIF2 DAC Playback Switch' on
amixer cset name='DAC Reversed Playback Switch' off
amixer cset name='AIF1 DA0 Stereo Playback Route' Mix Mono
amixer cset name='Earpiece Playback Switch' off
amixer cset name='Earpiece Source Playback Route' DACL

amixer cset name='Line In Capture Switch' off
amixer cset name='Mic1 Capture Switch' on
amixer cset name='Mic1 Boost Volume' 2
amixer cset name='Mic2 Capture Switch' off
amixer cset name='Mic2 Boost Volume' 2
amixer cset name='Mixer Capture Switch' off
amixer cset name='Mixer Reversed Capture Switch' off
amixer cset name='ADC Capture Volume' 160
amixer cset name='ADC Gain Capture Volume' 2
amixer cset name='AIF1 AD0 Capture Volume' 160
amixer cset name='AIF1 AD0 Mixer ADC Capture Switch' on
amixer cset name='AIF2 ADC Capture Volume' 160
amixer cset name='AIF2 ADC Mixer ADC Capture Switch' on
amixer cset name='AIF2 ADC Mixer AIF1 DA0 Capture Switch' off
amixer cset name='AIF2 ADC Mixer AIF2 DAC Rev Capture Switch' off
amixer cset name='AIF2 ADC Mixer AIF1 DA0 Capture Switch' off
amixer cset name='AIF2 ADC Mixer AIF1 DA0 Capture Switch' off

}


# Listen for calls...
interface=org.freedesktop.ModemManager1.Call
member=StateChanged

dbus-monitor --system "type='signal', interface='$interface', member='$member'" |
while read -r line; do
	# echo "$line"
	[[ "$line" =~ "$member" ]]; ret=$?
	if [ $ret -eq 0 ]; then
		# Get the 3 properties of call status
		read line1a line1b; read line2a line2b; read line3a line3b
		# echo "$line1a $line1b | $line2a $line2b | $line3a $line3b"
		# 2,4 is outgoing voice call connected
		# 3,4 is inconming voice call connected
		# 4,7 is voice call hung up
		if [ $line1b -eq 3 -a $line2b -eq 4 ] || [ $line1b -eq 2 -a $line2b -eq 4 ]; then
			enable_audio_on_call # Enable all digital paths to modem.
		elif [ $line1b -eq 4 -a $line2b -eq 7 ]; then
			enable_generic_audio_profile # Disable all digital paths to modem.
		fi
	fi
done
