Name:           pinephone-helpers
Version:        v0.1.0
Release:        1%{?dist}
Summary:        Pinephone call audio routing

License:        GPLv3+
URL:            https://gitlab.com/slem.os/%{name}
Source0:        callaudio.sh
Source1:        pinephone-boot.service
Source2:        pp-start.sh
Source3:        pp-stop.sh
Source4:        pinephone-calls.service
Source5:	20-pinephone.conf
Source6:        pp-setup.sh 
Source7:        pinephone-setup.service
Source8:        pinephone-suspend-hook.sh
Source9:        HiFi.conf
Source10:       VoiceCall.conf
Source11:       PinePhone.conf
Source12:       opensuse-background.jpg
Source13:       square-hicolor.svg
Source15:       90_pinephone.gschema.override
Source16:       90-modem-manager.rules
Source17:       91-network-manager.rules
Source18:       opensuse.json
Source19:       opensuse-profile.sh
Source20:       10-proximity.rules
Source21:       20-pinephone-led.rules
Source22:       phosh-renice.service
Source23:       phosh_renice.sh

BuildArch:      aarch64


BuildRequires:  systemd

%description
Turn on and off the modem automatically at boot, and listen for
new calls to automatically configure audio routing when detected.

%global debug_package %{nil}


%build


%install
mkdir -p $RPM_BUILD_ROOT/usr/bin
chmod +x %{SOURCE0}
cp %{SOURCE0} $RPM_BUILD_ROOT/usr/bin
chmod +x %{SOURCE2}
cp %{SOURCE2} $RPM_BUILD_ROOT/usr/bin
chmod +x %{SOURCE3}
cp %{SOURCE3} $RPM_BUILD_ROOT/usr/bin
chmod +x %{SOURCE6}
cp %{SOURCE6} $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT/etc/systemd/system
cp %{SOURCE4} $RPM_BUILD_ROOT/etc/systemd/system
cp %{SOURCE1} $RPM_BUILD_ROOT/etc/systemd/system
cp %{SOURCE7} $RPM_BUILD_ROOT/etc/systemd/system
mkdir -p $RPM_BUILD_ROOT/etc/NetworkManager/conf.d
cp %{SOURCE5} $RPM_BUILD_ROOT/etc/NetworkManager/conf.d
mkdir -p $RPM_BUILD_ROOT/usr/lib/systemd/system-sleep/
chmod +x %{SOURCE8}
cp %{SOURCE8} $RPM_BUILD_ROOT/usr/lib/systemd/system-sleep/
mkdir -p $RPM_BUILD_ROOT/usr/share/alsa/ucm2/PinePhone/
cp %{SOURCE9} $RPM_BUILD_ROOT/%{_datadir}/alsa/ucm2/PinePhone/
cp %{SOURCE10} $RPM_BUILD_ROOT/%{_datadir}/alsa/ucm2/PinePhone/
cp %{SOURCE11} $RPM_BUILD_ROOT/%{_datadir}/alsa/ucm2/PinePhone/
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/wallpapers
cp %{SOURCE12} $RPM_BUILD_ROOT/%{_datadir}/wallpapers/
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/icons/vendor/scalable/emblems/
cp %{SOURCE13} $RPM_BUILD_ROOT/%{_datadir}/icons/vendor/scalable/emblems/emblem-vendor.svg
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/glib-2.0/schemas/
cp %{SOURCE15} $RPM_BUILD_ROOT/%{_datadir}/glib-2.0/schemas/90_pinephone.gschema.override
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/polkit-1/rules.d/
cp %{SOURCE16} $RPM_BUILD_ROOT/%{_datadir}/polkit-1/rules.d/
cp %{SOURCE17} $RPM_BUILD_ROOT/%{_datadir}/polkit-1/rules.d/
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/feedbackd/themes
cp %{SOURCE18} $RPM_BUILD_ROOT/%{_datadir}/feedbackd/themes/
mkdir -p $RPM_BUILD_ROOT/etc/profile.d/
cp %{SOURCE19} $RPM_BUILD_ROOT/etc/profile.d/
mkdir -p $RPM_BUILD_ROOT/etc/udev/rules.d
cp %{SOURCE20} $RPM_BUILD_ROOT/etc/udev/rules.d/
cp %{SOURCE21} $RPM_BUILD_ROOT/etc/udev/rules.d/
cp %{SOURCE22} $RPM_BUILD_ROOT/etc/systemd/system/
chmod +x %{SOURCE23}
cp %{SOURCE23} $RPM_BUILD_ROOT/usr/bin/phosh_renice

%post
%systemd_post pinephone-boot.service
%systemd_post pinephone-calls.service
%systemd_post pinephone-setup.service
%systemd_post phosh-renice.service
systemctl enable pinephone-boot.service
systemctl enable pinephone-calls.service
systemctl enable pinephone-setup.service
systemctl enable phosh-renice.service


%preun
%systemd_preun pinephone-boot.service
%systemd_preun pinephone-calls.service
%systemd_preun pinephone-setup.service
%systemd_preun phosh-renice.service

%postun
%systemd_postun_with_restart pinephone-boot.service
%systemd_postun_with_restart pinephone-calls.service
%systemd_postun_with_restart pinephone-setup.service
%systemd_postun_with_restart phosh-renice.service

%files
%{_bindir}/*
%{_sysconfdir}/systemd/system/*
%dir %{_sysconfdir}/NetworkManager
%dir %{_sysconfdir}/NetworkManager/conf.d
%{_sysconfdir}/NetworkManager/conf.d/20-pinephone.conf
/usr/lib/systemd/system-sleep/pinephone-suspend-hook.sh
%dir %{_datadir}/alsa
%dir %{_datadir}/alsa/*
%{_datadir}/alsa/ucm2/PinePhone/
%dir %{_datadir}/wallpapers
%{_datadir}/wallpapers/*
%dir %{_datadir}/icons
%dir %{_datadir}/icons/vendor
%dir %{_datadir}/icons/vendor/scalable
%dir %{_datadir}/icons/vendor/scalable/emblems
%{_datadir}/icons/vendor/scalable/emblems/emblem-vendor.svg
%dir %{_datadir}/glib-2.0
%dir %{_datadir}/glib-2.0/schemas/
%{_datadir}/glib-2.0/schemas/90_pinephone.gschema.override
%dir %{_datadir}/polkit-1
%dir %{_datadir}/polkit-1/rules.d/
%{_datadir}/polkit-1/rules.d/*
%dir %{_datadir}/feedbackd/
%dir %{_datadir}/feedbackd/themes/
%{_datadir}/feedbackd/themes/opensuse.json
%dir /etc/profile.d/
/etc/profile.d/opensuse-profile.sh
%dir /etc/
%dir /etc/udev/
%dir /etc/udev/rules.d/
/etc/udev/rules.d/*

%changelog
